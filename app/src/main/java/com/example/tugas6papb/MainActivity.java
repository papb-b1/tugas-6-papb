package com.example.tugas6papb;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    RecyclerView rv;
    LinearLayoutManager llm;
    AdapterData adapterData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new BackgroundTask().execute();
        rv = findViewById(R.id.rv);
        llm = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(llm);
    }
    public class BackgroundTask extends AsyncTask<Void, Void, ArrayList<ArrayList<String>>>{
        @Override
        protected void onPostExecute(ArrayList<ArrayList<String>> strings) {
            super.onPostExecute(strings);
            adapterData = new AdapterData(MainActivity.this,strings);
            rv.setAdapter(adapterData);
            adapterData.notifyDataSetChanged();
        }

        @Override
        protected ArrayList<ArrayList<String>>  doInBackground(Void... voids) {
            RssParser rp = new RssParser();
            String xml ="";
            try {
                xml=rp.loadRssFromUrl("https://medium.com/feed/tag/programming");
                ArrayList<ArrayList<String>> strings = rp.parseRssFromUrl(xml);
                return strings;
            } catch (IOException e) {
                return null;
            } catch (ParserConfigurationException e) {
                return null;
            } catch (SAXException e) {
                return null;
            }


        }
    }
}
