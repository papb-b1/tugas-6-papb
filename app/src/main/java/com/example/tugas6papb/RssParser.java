package com.example.tugas6papb;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RssParser {
    private static String TAG_ITEM = "item";
    private static String TAG_TITLE = "title";
    private static String TAG_CHANNEL = "channel";
    private static  String TAG_LINK="link";

    public String loadRssFromUrl(String url) throws IOException {
        Log.d("RSSPARSER", "Start rss parser");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        String xml = response.body().string();
        Log.d("RSSPARSER", "xml " + xml);
        return xml;
    }

    public ArrayList<ArrayList<String>> parseRssFromUrl(String xml) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = builder.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document doc = db.parse(is);
        NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
        Element e = (Element) nodeList.item(0);

        NodeList items = e.getElementsByTagName(TAG_ITEM);
        for (int i = 0; i < items.getLength(); i++) {
            list.add(new ArrayList<String>());
            Element e1 = (Element) items.item(i);
            NodeList nJudul = e1.getElementsByTagName(TAG_TITLE);
            Node neJudul = nJudul.item(0);
            Node childJudul = neJudul.getFirstChild();

            NodeList  nLink= e1.getElementsByTagName(TAG_LINK);
            Node neLink = nLink.item(0);
            Node childLink = neLink.getFirstChild();

            String judul = childJudul.getNodeValue();
            Log.d("RSSPARSER", judul);
            list.get(i).add(judul);

            String link = childLink.getNodeValue();
            Log.d("RSSPARSER", link);
            list.get(i).add(link);
        }
        return list;
    }

}

